<?php 
namespace App\Controller;

use App\Entity\Property;
use App\Repository\PropertyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class PropertyController extends AbstractController
{

    /**
     * @Route("/biens", name="property.index" )
     * @param PropertyRepository $repository
     * @return Response
     */
    public function index(PropertyRepository $repository): Response
    {
        $property = $repository->findAllVisible();
        return $this->render('property/index.html.twig',[
            'current_menu'=> 'properties'
        ]);

    }


    /**
     * @Route("/biens/{slug}-{id}", name="property.show", requirements={"slug":"[a-z0-9\-]*"} )
     * @param PropertyRepository $repository
     * @param $slug
     * @param $id
     * @return Response
     */
    public function show(PropertyRepository $repository, $slug, $id): Response
    {
        $property = $repository->find($id);
        return $this->render('property/show.html.twig',[
            'property'=> $property,
            'current_menu'=> 'properties'
        ]);

    }
}